FROM ubuntu:20.04

# Run as root
USER root

# Install the PHP PPA
RUN apt update && \
    apt upgrade -y && \
    apt install -y nano git zsh iputils-ping net-tools python3-pip && \
    pip3 install ansible ansible-lint && \
    ansible-galaxy collection install community.general && \
    ansible-galaxy collection install kubernetes.core

# Cleanup the container image
RUN apt clean && apt autoclean -y && apt autoremove -y && rm -rf /var/lib/{apt,dpkg,cache,log}/
